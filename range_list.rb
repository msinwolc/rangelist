class RangeList
  def initialize(opt = {})
    @range_list = [[]]
  end

  def add(new_range = [])
    if !new_range.is_a?(Array) && !new_range[0].is_a?(Array)
      return
    end

    res = []
    i = 0
    @range_list = @range_list.compact
    len = @range_list.length

    if @range_list[0].length == 0
      @range_list[0] = new_range
    end

    while i < len && @range_list[i][1] < new_range[0]
      res << @range_list[i]
      i += 1
    end

    while i < len && @range_list[i][0] <= new_range[1]
      new_range[0] = [new_range[0], @range_list[i][0]].min
      new_range[1] = [new_range[1], @range_list[i][1]].max
      i += 1
    end
    res << new_range

    while i < len
      res << @range_list[i]
      i += 1
    end

    @range_list = res
  end

  def remove(target_range = [])
    if !target_range.is_a?(Array) && !target_range[0].is_a?(Array)
      return
    end

    res = []
    i = 0
    @range_list = @range_list.compact
    len = @range_list.length

    if @range_list[0].length == 0
      return
    end

    while i < len && @range_list[i][1] < target_range[0]
      res << @range_list[i]
      i += 1
    end

    while i < len && @range_list[i][0] < target_range[1]
      if @range_list[i][0] < target_range[0]
        res << [@range_list[i][0], target_range[0]]
      elsif @range_list[i][1] < target_range[1]
        i += 1
        next
      end
      i += 1
    end

    if @range_list[i - 1][1] > target_range[1]
      res << [target_range[1], @range_list[i - 1][1]]
    end

    while i < len
      res << @range_list[i]
      i += 1
    end

    @range_list = res
  end

  def print
    str = ""
    @range_list.each do |range|
      str += "[#{range.first}, #{range.last}) "
    end
    puts str
  end
end