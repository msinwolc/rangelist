require './range_list.rb'

def test(*args)
  rl = RangeList.new

  puts "add [1, 5]"
  rl.add([1, 5])
  rl.print

  puts "add [10, 20]"
  rl.add([10, 20])
  rl.print

  puts "add [20, 20]"
  rl.add([20, 20])
  rl.print

  puts "add [20, 21]"
  rl.add([20, 21])
  rl.print

  puts "add [2, 4]"
  rl.add([2, 4])
  rl.print

  puts "add [3, 8]"
  rl.add([3, 8])
  rl.print

  puts "add [9, 12]"
  rl.add([9, 12])
  rl.print

  puts "remove [10, 10]"
  rl.remove([10, 10])
  rl.print

  puts "remove [7, 11]"
  rl.remove([7, 11])
  rl.print

  puts "remove [15, 17]"
  rl.remove([15, 17])
  rl.print

  puts "remove [3, 19]"
  rl.remove([3, 19])
  rl.print
end

test()